/datum/map/untold/setup_map()
	..()
	system_name = generate_system_name()
	minor_announcement = new(new_sound = sound('sound/AI/commandreport.ogg', volume = 45))

/datum/map/untold/get_map_info()
	. = list()
	. +=  "You're aboard the " + replacetext("<b>[station_name]</b>", "\improper", "") + ", an old facility owned by the now defunct NanoTrasen. Its primary mission was research and development in Phoron, and general exploration along the way."
	. +=  "The vessel was staffed with a mix of SCG government personnel and hired contractors, who have all since left this post leaving you and the few others in the cryopods now to search for others and answers."
	. +=  "This area of space was charted, and mixed SCG territory and the Sigmar Concord. You might encounter old forgotten outposts or drifting hulks, but no recognized government holds claim anywhere it seems these days..."
	return jointext(., "<br>")

/datum/map/untold/send_welcome()
	var/obj/effect/overmap/visitable/ship/untold = SSshuttle.ship_by_type(/obj/effect/overmap/visitable/ship/untold)

	var/welcome_text = "<center><img src = sollogo.png /><br /><font size = 3><b>SEV untold</b> Sensor Readings:</font><br>"
	welcome_text += "Report generated on [stationdate2text()] at [stationtime2text()]</center><br /><br />"
	welcome_text += "<hr>Current system:<br /><b>[untold ? system_name() : "Unknown"]</b><br /><br>"

	if (untold) //If the overmap is disabled, it's possible for there to be no untold.
		var/list/space_things = list()
		welcome_text += "Current Coordinates:<br /><b>[untold.x]:[untold.y]</b><br /><br>"
		welcome_text += "Next system targeted for jump:<br /><b>[generate_system_name()]</b><br /><br>"
		welcome_text += "Travel time to Sol:<br /><b>[rand(15,45)] days</b><br /><br>"
		welcome_text += "Time since last port visit:<br /><b>[rand(60,180)] days</b><br /><hr>"
		welcome_text += "Scan results show the following points of interest:<br />"

		for(var/zlevel in map_sectors)
			var/obj/effect/overmap/visitable/O = map_sectors[zlevel]
			if(O.name == untold.name)
				continue
			if(istype(O, /obj/effect/overmap/visitable/ship/landable)) //Don't show shuttles
				continue
			if (O.hide_from_reports)
				continue
			space_things |= O

		var/list/distress_calls
		for(var/obj/effect/overmap/visitable/O in space_things)
			var/location_desc = " at present co-ordinates."
			if(O.loc != untold.loc)
				var/bearing = round(90 - Atan2(O.x - untold.x, O.y - untold.y),5) //fucking triangles how do they work
				if(bearing < 0)
					bearing += 360
				location_desc = ", bearing [bearing]."
			if(O.has_distress_beacon)
				LAZYADD(distress_calls, "[O.has_distress_beacon][location_desc]")
			welcome_text += "<li>\A <b>[O.name]</b>[location_desc]</li>"

		if(LAZYLEN(distress_calls))
			welcome_text += "<br><b>Distress calls logged:</b><br>[jointext(distress_calls, "<br>")]<br>"
		else
			welcome_text += "<br>No distress calls logged.<br />"
		welcome_text += "<hr>"

	post_comm_message("Alderis Station Sensor Readings", welcome_text)
	minor_announcement.Announce(message = "New [GLOB.using_map.company_name] Update available at all communication consoles.")
