// Alien clothing.

// Unathi clothing
/datum/gear/suit/unathi/
	sort_category = "Xenowear - Unathi"
	category = /datum/gear/suit/unathi/
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/suit/unathi/mantle
	display_name = "hide mantle (Unathi)"
	path = /obj/item/clothing/suit/unathi/mantle
	cost = 1

/datum/gear/suit/unathi/robe
	display_name = "roughspun robe (Unathi)"
	path = /obj/item/clothing/suit/unathi/robe
	cost = 1

/datum/gear/suit/unathi/knifeharness
	display_name = "decorated harness"
	path = /obj/item/clothing/accessory/storage/knifeharness
	cost = 5

/datum/gear/suit/unathi/savage_hunter
	display_name = "savage hunter hides (Male, Unathi)"
	path = /obj/item/clothing/under/savage_hunter
	slot = slot_w_uniform
	cost = 2

/datum/gear/suit/unathi/savage_hunter/female
	display_name = "savage hunter hides (Female, Unathi)"
	path = /obj/item/clothing/under/savage_hunter/female
	slot = slot_w_uniform
	cost = 2

//Skrell Chains
/datum/gear/ears/skrell/
	sort_category = "Xenowear - Skrell"
	category = /datum/gear/ears/skrell/
	whitelisted = list(SPECIES_SKRELL)

/datum/gear/ears/skrell/chains
	display_name = "headtail chain selection (Skrell)"
	path = /obj/item/clothing/ears/skrell/chain
	flags = GEAR_HAS_SUBTYPE_SELECTION

/datum/gear/ears/skrell/colored/chain
	display_name = "colored headtail chain, colour select (Skrell)"
	path = /obj/item/clothing/ears/skrell/colored/chain
	flags = GEAR_HAS_COLOR_SELECTION

//Skrell Bands
/datum/gear/ears/skrell/bands
	display_name = "headtail band selection (Skrell)"
	path = /obj/item/clothing/ears/skrell/band
	flags = GEAR_HAS_SUBTYPE_SELECTION

/datum/gear/ears/skrell/colored/band
	display_name = "headtail bands, colour select (Skrell)"
	path = /obj/item/clothing/ears/skrell/colored/band
	flags = GEAR_HAS_COLOR_SELECTION

//Skrell Cloth
/datum/gear/ears/skrell/cloth/male
	display_name = "male headtail cloth (Skrell)"
	path = /obj/item/clothing/ears/skrell/cloth_male
	flags = GEAR_HAS_COLOR_SELECTION


/datum/gear/ears/skrell/cloth/female
	display_name = "female headtail cloth (Skrell)"
	path = /obj/item/clothing/ears/skrell/cloth_female
	flags = GEAR_HAS_COLOR_SELECTION

/datum/gear/head/skrell_helmet
	display_name = "Skrellian helmet"
	path = /obj/item/clothing/head/helmet/skrell
	whitelisted = list(SPECIES_SKRELL)
	sort_category = "Xenowear - Skrell"
	allowed_roles = list(/datum/job/hos, /datum/job/warden, /datum/job/officer, /datum/job/detective)

/datum/gear/accessory/skrell_badge
	display_name = "skrellian SDTF badge"
	path = /obj/item/clothing/accessory/badge/tags/skrell
	whitelisted = list(SPECIES_SKRELL)
	sort_category = "Xenowear - Skrell"

// IPC clothing
/datum/gear/mask/ipc_monitor
	display_name = "display monitor (IPC)"
	path = /obj/item/clothing/mask/monitor
	sort_category = "Xenowear - IPC"
	whitelisted = list(SPECIES_IPC)
	cost = 0

/datum/gear/suit/lab_xyn_machine
	display_name = "Xynergy labcoat"
	path = /obj/item/clothing/suit/storage/toggle/labcoat/xyn_machine
	slot = slot_wear_suit
	sort_category = "Xenowear - IPC"
	whitelisted = list(SPECIES_IPC)

// Misc clothing
/datum/gear/uniform/harness
	display_name = "gear harness (Full Body Prosthetic, Diona, Giant Armoured Serpentid)"
	path = /obj/item/clothing/under/harness
	sort_category = "Xenowear - IPC"
	whitelisted = list(SPECIES_IPC,SPECIES_DIONA, SPECIES_NABBER)

/datum/gear/shoes/toeless
	display_name = "toeless jackboots"
	path = /obj/item/clothing/shoes/jackboots/unathi
	sort_category = "Xenowear"

/datum/gear/shoes/wrk_toeless
	display_name = "toeless workboots"
	path = /obj/item/clothing/shoes/workboots/toeless
	sort_category = "Xenowear"

// Pre-modified gloves

/datum/gear/gloves/colored/modified
	display_name = "modified gloves, colored"
	path = /obj/item/clothing/gloves/color/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/latex/modified
	display_name = "modified gloves, latex"
	path = /obj/item/clothing/gloves/latex/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/nitrile/modified
	display_name = "modified gloves, nitrile"
	path = /obj/item/clothing/gloves/latex/nitrile/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/rainbow/modified
	display_name = "modified gloves, rainbow"
	path = /obj/item/clothing/gloves/rainbow/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/evening/modified
	display_name = "modified gloves, evening"
	path = /obj/item/clothing/gloves/color/evening/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/botany/modified
	display_name = "modified gloves, botany"
	path = /obj/item/clothing/gloves/thick/botany/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

/datum/gear/gloves/work/modified
	display_name = "modified gloves, work"
	path = /obj/item/clothing/gloves/thick/modified
	sort_category = "Xenowear - Unathi"
	whitelisted = list(SPECIES_UNATHI, SPECIES_YEOSA)

// Vox clothing
/datum/gear/mask/gas/vox
	display_name = "vox breathing mask"
	path = /obj/item/clothing/mask/gas/vox
	sort_category = "Xenowear - Vox"
	whitelisted = list(SPECIES_VOX, SPECIES_VOX_ARMALIS)

// Space-Adapted Human clothing
/datum/gear/accessory/space_adapted
	sort_category = "Xenowear"
	category = /datum/gear/accessory/space_adapted
	whitelisted = list(SPECIES_SPACER)

/datum/gear/accessory/space_adapted/venter
	display_name = "venter assembly"
	path = /obj/item/clothing/accessory/space_adapted/venter
	flags = GEAR_HAS_COLOR_SELECTION

/datum/gear/accessory/space_adapted/legbrace
	display_name = "legbrace"
	path = /obj/item/clothing/accessory/space_adapted/bracer
	flags = GEAR_HAS_COLOR_SELECTION

/datum/gear/accessory/space_adapted/neckbrace
	display_name = "neckbrace"
	path = /obj/item/clothing/accessory/space_adapted/bracer/neckbrace
	flags = GEAR_HAS_COLOR_SELECTION

//teshari
/datum/gear/uniform/teshari/smock
	display_name = "smock selection (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/under/teshari/smock
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/uniform/teshari/smock/New()
	..()
	var/smock = list()
	smock += /obj/item/clothing/under/teshari/smock
	smock += /obj/item/clothing/under/teshari/smock/red
	smock += /obj/item/clothing/under/teshari/smock/white
	smock += /obj/item/clothing/under/teshari/smock/yellow
	smock += /obj/item/clothing/under/teshari/smock/rainbow
	gear_tweaks += new/datum/gear_tweak/path/specified_types_list(smock)

/datum/gear/uniform/teshari/uniform
	display_name = "uniform selection (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/under/teshari/uniform
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/uniform/teshari/uniform/New()
	..()
	var/uniform = list()
	uniform += /obj/item/clothing/under/teshari/uniform
	uniform += /obj/item/clothing/under/teshari/uniform/bluegrey
	uniform += /obj/item/clothing/under/teshari/uniform/black
	uniform += /obj/item/clothing/under/teshari/uniform/med
	uniform += /obj/item/clothing/under/teshari/uniform/med/alt
	uniform += /obj/item/clothing/under/teshari/uniform/sci
	uniform += /obj/item/clothing/under/teshari/uniform/sci/alt
	uniform += /obj/item/clothing/under/teshari/uniform/sec
	uniform += /obj/item/clothing/under/teshari/uniform/sec/alt
	uniform += /obj/item/clothing/under/teshari/uniform/engi
	uniform += /obj/item/clothing/under/teshari/uniform/engi/alt
	uniform += /obj/item/clothing/under/teshari/uniform/cap
	uniform += /obj/item/clothing/under/teshari/uniform/cap/alt
	uniform += /obj/item/clothing/under/teshari/uniform/cap/alt2
	uniform += /obj/item/clothing/under/teshari/uniform/black
	gear_tweaks += new/datum/gear_tweak/path/specified_types_list(uniform)

/datum/gear/uniform/teshari/worksuit
	display_name = "worksuit selection (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/under/teshari/worksuit
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/uniform/teshari/worksuit/New()
	..()
	var/worksuit = list()
	worksuit += /obj/item/clothing/under/teshari/worksuit
	worksuit += /obj/item/clothing/under/teshari/worksuit/blackpurple
	worksuit += /obj/item/clothing/under/teshari/worksuit/blackorange
	worksuit += /obj/item/clothing/under/teshari/worksuit/blackblue
	worksuit += /obj/item/clothing/under/teshari/worksuit/blackgreen
	worksuit += /obj/item/clothing/under/teshari/worksuit/whitered
	worksuit += /obj/item/clothing/under/teshari/worksuit/whitepurple
	worksuit += /obj/item/clothing/under/teshari/worksuit/whiteorange
	worksuit += /obj/item/clothing/under/teshari/worksuit/whiteblue
	worksuit += /obj/item/clothing/under/teshari/worksuit/whitegreen
	gear_tweaks += new/datum/gear_tweak/path/specified_types_list(worksuit)

/datum/gear/shoes/footwraps
	display_name = "cloth footwaraps (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/shoes/footwraps
	flags = GEAR_HAS_COLOR_SELECTION
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/shoes/teshboots
	display_name = "small workboots (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/shoes/workboots/teshari
	cost = 2
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/eyes/teshlens
	display_name = "eye lenses (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/glasses/sunglasses/lenses
	cost = 2
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/eyes/teshlenssec
	display_name = "security HUD lenses (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/glasses/sunglasses/sechud/lenses
	cost = 3
	whitelisted = list(SPECIES_TESHARI)

/datum/gear/eyes/teshlensmed
	display_name = "medical HUD lenses (Teshari)"
	sort_category = "Xenowear - Teshari"
	path = /obj/item/clothing/glasses/hud/health/lenses
	cost = 3
	whitelisted = list(SPECIES_TESHARI)