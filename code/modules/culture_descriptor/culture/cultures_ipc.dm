/decl/cultural_info/culture/ipc
	name = CULTURE_POSITRONICS
	description = "Union members are a significant chunk of the positronic population, belonging to a \
	group of rebels started by Proteus and five hundred of his allies. Their primary goals, aside from \
	the expansion of the Union, mostly revolve around freeing other synthetics from organic ownership. \
	They can be viewed as dangerous radicals by lawed synthetics, though most begrudgingly accept their aid."
	language = LANGUAGE_EAL
	secondary_langs = list(
//Human
LANGUAGE_HUMAN_EURO,     // "Zurich Accord Common"
LANGUAGE_HUMAN_CHINESE,  // "Yangyu"
LANGUAGE_HUMAN_ARABIC, // "Prototype Standard Arabic"
LANGUAGE_HUMAN_INDIAN,   // "New Dehlavi"
LANGUAGE_HUMAN_IBERIAN,  // "Iberian"
LANGUAGE_HUMAN_RUSSIAN,  // "Pan-Slavic"
LANGUAGE_HUMAN_SELENIAN, // "Selenian"

//Human misc
LANGUAGE_GUTTER,        // "Gutter"
LANGUAGE_LEGALESE,      // "Legalese"
LANGUAGE_SPACER,        // "Spacer"

//Alien
LANGUAGE_EAL,             //  "Encoded Audio Language"
LANGUAGE_UNATHI_SINTA,    // "Sinta'unathi"
LANGUAGE_UNATHI_YEOSA,    //  "Yeosa'unathi"
LANGUAGE_SKRELLIAN,       //  "Skrellian"
LANGUAGE_ROOTLOCAL,       //  "Local Rootspeak"
LANGUAGE_ROOTGLOBAL,      //  "Global Rootspeak"
LANGUAGE_ADHERENT,        //  "Protocol"
LANGUAGE_VOX,             //  "Vox-pidgin"
LANGUAGE_NABBER,          //  "Serpentid"
LANGUAGE_TESHARI,		//	"Schechi"
LANGUAGE_CANILUNZT,        //  "Canilunzt"
//Antag
//LANGUAGE_CULT             // "Cult"
//LANGUAGE_CULT_GLOBAL      // "Occult"
//LANGUAGE_ALIUM           //  "Alium"

//Other
//LANGUAGE_PRIMITIVE        // "Primitive"
LANGUAGE_SIGN,             // "Sign Language"
//LANGUAGE_ROBOT_GLOBAL     // "Robot Talk"
//LANGUAGE_DRONE_GLOBAL     // "Drone Talk"
//LANGUAGE_CHANGELING_GLOBAL// "Changeling"
//LANGUAGE_BORER_GLOBAL     // "Cortical Link"
LANGUAGE_MANTID_NONVOCAL,  // "Ascent-Glow"
LANGUAGE_MANTID_VOCAL     // "Ascent-Voc"
//LANGUAGE_MANTID_BROADCAST // "Worldnet"
//LANGUAGE_ZOMBIE           // "Zombie"
	)

/decl/cultural_info/culture/ipc/sanitize_name(var/new_name)
	return sanitizeName(new_name, allow_numbers = 1)
