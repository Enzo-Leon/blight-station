// See specific map job files for valid jobs. They use types so cannot be compiled at this level.
/decl/cultural_info/culture/nabber
	name = CULTURE_NABBER_CMINUS

	description = "You have been trained to Xynergy Grade PLACEHOLDER."
	language = LANGUAGE_NABBER
	secondary_langs = list(
//Human
LANGUAGE_HUMAN_EURO,     // "Zurich Accord Common"
LANGUAGE_HUMAN_CHINESE,  // "Yangyu"
LANGUAGE_HUMAN_ARABIC, // "Prototype Standard Arabic"
LANGUAGE_HUMAN_INDIAN,   // "New Dehlavi"
LANGUAGE_HUMAN_IBERIAN,  // "Iberian"
LANGUAGE_HUMAN_RUSSIAN,  // "Pan-Slavic"
LANGUAGE_HUMAN_SELENIAN, // "Selenian"

//Human misc
LANGUAGE_GUTTER,        // "Gutter"
LANGUAGE_LEGALESE,      // "Legalese"
LANGUAGE_SPACER,        // "Spacer"

//Alien
LANGUAGE_EAL,             //  "Encoded Audio Language"
LANGUAGE_UNATHI_SINTA,    // "Sinta'unathi"
LANGUAGE_UNATHI_YEOSA,    //  "Yeosa'unathi"
LANGUAGE_SKRELLIAN,       //  "Skrellian"
LANGUAGE_ROOTLOCAL,       //  "Local Rootspeak"
LANGUAGE_ROOTGLOBAL,      //  "Global Rootspeak"
LANGUAGE_ADHERENT,        //  "Protocol"
LANGUAGE_VOX,             //  "Vox-pidgin"
LANGUAGE_NABBER,          //  "Serpentid"
LANGUAGE_TESHARI,		//	"Schechi"
LANGUAGE_CANILUNZT,        //  "Canilunzt"
//Antag
//LANGUAGE_CULT             // "Cult"
//LANGUAGE_CULT_GLOBAL      // "Occult"
//LANGUAGE_ALIUM           //  "Alium"

//Other
//LANGUAGE_PRIMITIVE        // "Primitive"
LANGUAGE_SIGN,             // "Sign Language"
//LANGUAGE_ROBOT_GLOBAL     // "Robot Talk"
//LANGUAGE_DRONE_GLOBAL     // "Drone Talk"
//LANGUAGE_CHANGELING_GLOBAL// "Changeling"
//LANGUAGE_BORER_GLOBAL     // "Cortical Link"
LANGUAGE_MANTID_NONVOCAL,  // "Ascent-Glow"
LANGUAGE_MANTID_VOCAL     // "Ascent-Voc"
//LANGUAGE_MANTID_BROADCAST // "Worldnet"
//LANGUAGE_ZOMBIE           // "Zombie"
	)
	var/list/valid_jobs = list()
	var/list/hidden_valid_jobs = list(/datum/job/ai, /datum/job/cyborg)
	var/title_suffix

/decl/cultural_info/culture/nabber/get_formal_name_suffix()
	return title_suffix

/decl/cultural_info/culture/nabber/New()
	..()

	// Make sure this will show up in the manifest and on IDs.
	title_suffix = " ([name])"

	// Update our desc based on available jobs for this rank.
	var/list/job_titles = list()
	for(var/jobtype in valid_jobs)
		var/datum/job/job = jobtype
		LAZYADD(job_titles, initial(job.title))
	if(!LAZYLEN(job_titles))
		LAZYADD(job_titles, "none")
	description = "You have been trained by Xynergy to [name]. This makes you suitable for the following roles: [english_list(job_titles)]."

	// Set up our qualifications.
	LAZYADD(qualifications, "<b>[name]</b>")
	for(var/role in job_titles)
		LAZYADD(qualifications, "Safe for [role].")

	// Add our hidden jobs since we're done building the desc.
	if(LAZYLEN(hidden_valid_jobs))
		LAZYADD(valid_jobs, hidden_valid_jobs)

/decl/cultural_info/culture/nabber/c
	name = CULTURE_NABBER_C
	valid_jobs = list(/datum/job/janitor)

/decl/cultural_info/culture/nabber/c/plus
	name = CULTURE_NABBER_CPLUS

/decl/cultural_info/culture/nabber/b
	name = CULTURE_NABBER_B
	valid_jobs = list(/datum/job/bartender, /datum/job/chef)

/decl/cultural_info/culture/nabber/b/minus
	name = CULTURE_NABBER_BMINUS

/decl/cultural_info/culture/nabber/b/plus
	name = CULTURE_NABBER_BPLUS

/decl/cultural_info/culture/nabber/a
	name = CULTURE_NABBER_A
	valid_jobs = list(/datum/job/chemist, /datum/job/roboticist)

/decl/cultural_info/culture/nabber/a/minus
	name = CULTURE_NABBER_AMINUS

/decl/cultural_info/culture/nabber/a/plus
	name = CULTURE_NABBER_APLUS
