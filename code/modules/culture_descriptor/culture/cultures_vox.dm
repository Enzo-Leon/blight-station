/decl/cultural_info/culture/vox
	name = CULTURE_VOX_ARKSHIP
	description = "The vast majority of vox are born and die on the enormous moon-sized arkships that form the only \
	permanent home any of the creatures have. Tending to the vast, decaying vessels is a full-time job for crews of thousands, \
	and although the glamour and allure of the raider life appeals to many, staying home is seen as a more responsible and mature \
	pursuit than haring off across the stars to attack the meat and steal their goods."
	hidden_from_codex = TRUE
	language = LANGUAGE_VOX
	secondary_langs = list(
//Human
LANGUAGE_HUMAN_EURO,     // "Zurich Accord Common"
LANGUAGE_HUMAN_CHINESE,  // "Yangyu"
LANGUAGE_HUMAN_ARABIC, // "Prototype Standard Arabic"
LANGUAGE_HUMAN_INDIAN,   // "New Dehlavi"
LANGUAGE_HUMAN_IBERIAN,  // "Iberian"
LANGUAGE_HUMAN_RUSSIAN,  // "Pan-Slavic"
LANGUAGE_HUMAN_SELENIAN, // "Selenian"

//Human misc
LANGUAGE_GUTTER,        // "Gutter"
LANGUAGE_LEGALESE,      // "Legalese"
LANGUAGE_SPACER,        // "Spacer"

//Alien
LANGUAGE_EAL,             //  "Encoded Audio Language"
LANGUAGE_UNATHI_SINTA,    // "Sinta'unathi"
LANGUAGE_UNATHI_YEOSA,    //  "Yeosa'unathi"
LANGUAGE_SKRELLIAN,       //  "Skrellian"
LANGUAGE_ROOTLOCAL,       //  "Local Rootspeak"
LANGUAGE_ROOTGLOBAL,      //  "Global Rootspeak"
LANGUAGE_ADHERENT,        //  "Protocol"
LANGUAGE_VOX,             //  "Vox-pidgin"
LANGUAGE_NABBER,          //  "Serpentid"
LANGUAGE_TESHARI,		//	"Schechi"
LANGUAGE_CANILUNZT,        //  "Canilunzt"
//Antag
//LANGUAGE_CULT             // "Cult"
//LANGUAGE_CULT_GLOBAL      // "Occult"
//LANGUAGE_ALIUM           //  "Alium"

//Other
//LANGUAGE_PRIMITIVE        // "Primitive"
LANGUAGE_SIGN,             // "Sign Language"
//LANGUAGE_ROBOT_GLOBAL     // "Robot Talk"
//LANGUAGE_DRONE_GLOBAL     // "Drone Talk"
//LANGUAGE_CHANGELING_GLOBAL// "Changeling"
//LANGUAGE_BORER_GLOBAL     // "Cortical Link"
LANGUAGE_MANTID_NONVOCAL,  // "Ascent-Glow"
LANGUAGE_MANTID_VOCAL     // "Ascent-Voc"
//LANGUAGE_MANTID_BROADCAST // "Worldnet"
//LANGUAGE_ZOMBIE           // "Zombie"
	)

/decl/cultural_info/culture/vox/salvager
	name = CULTURE_VOX_SALVAGER
	description = "The arkships, vast as they are, could not survive without the ceaseless efforts of the salvage crews \
	that strip-mine asteroids, stations and ships for the raw materials needed to keep things together. Although it is a \
	much less lethal pursuit than being a raider, salvagers have a dangerous job and often do not make it back to the ark \
	in one piece."

/decl/cultural_info/culture/vox/raider
	name = CULTURE_VOX_RAIDER
	description = "Amongst the vox, the prestige of being a raider is second only to working directly for an apex. They are \
	the cutting talon of the vox and the dashing, charismatic figures of adventure that serve as examples for fresh hatchlings \
	and ambitious labourers. Many raiders end up in positions of authority and power amid the ramshackle social structures of \
	the arkships, though as always they remain under the authority of the local apex."
